/**
 * @author Benjamin Collins / https://dashgl.com
 */


//------------------------------------------------------------------------------
// Collada Exporter
//------------------------------------------------------------------------------

THREE.ColladaExporter = function() {

	var date = new Date();

	this.vertices = [];
	this.skinIndices = [];
	this.skinWeights = [];

	this.normals = [];
	this.uvs = [];
	this.indices = [];

	this.output = {
		COLLADA: {
			_version: "1.4.1",
			_xmlns: "http://www.collada.org/2005/11/COLLADASchema",
			asset: {
				contributor: {
					authoring_tool: "Threejs Collada Exporter",
				},
				created: date.toUTCString(),
				up_axis: "Y_UP"
			},
			library_controllers: {},
			library_effects: {},
			library_geometries: {},
			library_images: {},
			library_materials: {},
			library_visual_scenes: {},
			scene: {
				instance_visual_scene: {
					_url: "#DefaultScene"
				}
			}
		}
	};


};

THREE.ColladaExporter.supportedTypes = [
	"Mesh",
	"SkinnedMesh"
];

THREE.ColladaExporter.prototype = {

	constructor: THREE.ColladaExporter,

	parse: function(input, options) {

		if (THREE.ColladaExporter.supportedTypes.indexOf(input.type) === -1) {
			throw new Error("THREE.ColladaExporter unsupported type");
		}

		// First step is to set comments and origin data

		this.output.COLLADA.asset.comments = "DashGL.com a site for OpenGL tutorials";
		this.output.COLLADA.asset.source_data = "Filename and offset";

		// Populate vertices

		console.log(input.geometry.vertices);

		for(let i = 0; i < input.geometry.vertices.length; i++) {
			let x = input.geometry.vertices[i].x.toFixed(2);
			let y = input.geometry.vertices[i].y.toFixed(2);
			let z = input.geometry.vertices[i].z.toFixed(2);
			this.vertices.push(x, y, z);
			this.skinIndices.push(input.geometry.skinIndices[i]);
			this.skinWeights.push(input.geometry.skinWeights[i]);
		}

		// Populate normals, uv and indices

		for(var i = 0; i < input.geometry.faces.length; i++) {
		
			let face = input.geometry.faces[i];
			let normal = input.geometry.faces[i].normal;
			let uv = input.geometry.faceVertexUvs[0][i];
			let indices = [face.a, face.b, face.c];	
			// Normals

			let nx = normal.x.toString(5);
			let ny = normal.y.toString(5);
			let nz = normal.z.toString(5);
			this.normals.push(nx, ny, nz);

			let n_index = this.normals.length / 3;
			
			// Texture coords
			
			for(var k = 0; k < uv.length; k++) {
				let u = (uv[k].x).toFixed(5);
				let v = (1 - uv[k].y).toFixed(5);

				let uv_index = this.uvs.length / 2;
				this.uvs.push(u, v);

				this.indices.push(indices[k], n_index, uv_index);
			}

		}

		// Next step is to look at the mesh to define our scene

		this.output.COLLADA.library_visual_scenes.visual_scene = {
			"node": {
				"instance_geometry": {
					"bind_material": {
						"technique_common": {
							"instance_material": {
								"_symbol": "Material-Red",
								"_target": "#Material-Red"
							}
						}
					},
					"_url": "#Geometry-mesh"
				},
				"_id": "Geometry-Node",
				"_name": "Mesh",
				"_type": "NODE"
			},
			"_id": "DefaultScene"
		};

		// Go in order from here, start with Effects

		this.output.COLLADA.library_materials.material = {
			"_id": "Material-Red",
			"_name": "Red",
			"instance_effect": {
				"_url": "#Effect-Red"
			}
		};

		// Library Effects

		this.output.COLLADA.library_effects.effect = {
			"_id": "Effect-Red",
			"_name": "Red",
			"profile_COMMON": {
				"technique": {
					"_sid": "common",
					"phong": {
						"diffuse": {
							"color": {
								"_sid": "diffuse_effect_rgb",
								"__text": "0.0 0.0 0.8 1"
							}
						}
					}
				}
			}
		};

		// Geometry (will need to split later)

		this.output.COLLADA.library_geometries.geometry = {
			"mesh": {
				"source": [],
				"vertices": {
					"input": {
						"_semantic": "POSITION",
						"_source": "#Geometry-mesh-positions"
					},
					"_id": "Geometry-mesh-vertices"
				}
			},
			"_id": "Geometry-mesh",
			"_name": "Mesh"
		};

		this.output.COLLADA.library_geometries.geometry.mesh.source.push({
			"float_array": {
				"_id": "Geometry-mesh-positions-array",
				"_count": this.vertices.length.toString(),
				"__text": this.vertices.join(" ")
			},
			"technique_common": {
				"accessor": {
					"param": [{
							"_name": "X",
							"_type": "float"
						},
						{
							"_name": "Y",
							"_type": "float"
						},
						{
							"_name": "Z",
							"_type": "float"
						}
					],
					"_count": (this.vertices.length / 3).toString(),
					"_source": "#Geometry-mesh-positions-array",
					"_stride": "3"
				}
			},
			"_id": "Geometry-mesh-positions",
			"_name": "positions"
		});

		this.output.COLLADA.library_geometries.geometry.mesh.source.push({
			"float_array": {
				"_id": "Geometry-mesh-normals-array",
				"_count": this.normals.length.toString(),
				"__text": this.normals.join(" "),
			},
			"technique_common": {
				"accessor": {
					"param": [{
							"_name": "X",
							"_type": "float"
						},
						{
							"_name": "Y",
							"_type": "float"
						},
						{
							"_name": "Z",
							"_type": "float"
						}
					],
					"_count": (this.normals.length / 3).toString(),
					"_source": "#Geometry-mesh-normals-array",
					"_stride": "3"
				}
			},
			"_id": "Geometry-mesh-normals",
			"_name": "normals"
		});

		this.output.COLLADA.library_geometries.geometry.mesh.source.push({
			"float_array": {
				"_id": "Geometry-mesh-Texture-array",
				"_count": this.uvs.length.toString(),
				"__text": this.uvs.join(" "),
			},
			"technique_common": {
				"accessor": {
					"param": [{
							"_name": "S",
							"_type": "float"
						},
						{
							"_name": "T",
							"_type": "float"
						}
					],
					"_count": (this.uvs.length / 2).toString(),
					"_source": "#Geometry-mesh-Texture-array",
					"_stride": "2"
				}
			},
			"_id": "Geometry-mesh-Texture",
			"_name": "Texture"
		});

		this.output.COLLADA.library_geometries.geometry.mesh.triangles = {
			"input": [
				{
					"_semantic": "VERTEX",
					"_source": "#Geometry-mesh-vertices",
					"_offset": "0"
				},
				{
					"_semantic": "NORMAL",
					"_source": "#Geometry-mesh-normals",
					"_offset": "1"
				},
				{
					"_semantic": "TEXCOORD",
					"_source": "#Geometry-mesh-Texture",
					"_offset": "2",
					"_set": "0"
				}
			],
			"p": this.indices.join(" "),
			"_count": (this.indices.length / 9),
			"_material": "Material-Red"
		};

		var x2js = new X2JS();
		var xmlAsStr = x2js.json2xml_str(this.output);
		return xmlAsStr;

	}

};
